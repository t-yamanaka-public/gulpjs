
const { PATH } = require('./path')
const { src, dest, lastRun } = require('gulp')

// copy
function copy () {
  return src([
    `${PATH.SRC}**/*.{php,html,tsv,csv,xml,webmanifest}`,
    `${PATH.SRC}.htaccess*`,
    `${PATH.SRC}.htpasswd*`
  ], {
    since: lastRun(copy)
  }).pipe(dest([PATH.DIST]))
}
exports.copy = copy

// copy assets
function copyAssets () {
  return src([
    `${PATH.SRC}**/*.{css,otf,ttf,eot,woff,woff2,pdf,json,jsonp,mp4,js,zip}`,
    `!${PATH.ASSETS.SRC}**/_*.js`,
    `!${PATH.ASSETS.SRC}js/bundle.js`
  ], {
    since: lastRun(copyAssets)
  })
    .pipe(dest(PATH.DIST))
    // .pipe(dest(PATH.ASSETS.WPDIST))
}
exports.copyAssets = copyAssets

// copy images
function copyImg () {
  return src([
    `${PATH.SRC}**/*.{svg,png,jpg,gif,pdf,ico}`,
    `!${PATH.ASSETS.SRC}img/svg-sprite/*.svg`,
    `!${PATH.ASSETS.SRC}img/svg-sprite-attr/*.svg`
    // `${PATH.ASSETS.SRC}**/bg_hero-top_*.jpg`
  ], {
    since: lastRun(copyImg)
  })
    .pipe(dest(PATH.DIST))
    // .pipe(dest(PATH.ASSETS.WPDIST))
}
exports.copyImg = copyImg
