
const { PATH } = require('./path')
const del = require('del')

// clean
function clean () {
  return del([
    `${PATH.DIST}**/*`
  ])
}
exports.clean = clean

// cleanMaps
function cleanMaps () {
  return del([
    `${PATH.ASSETS.DIST}maps`
  ])
}
exports.cleanMaps = cleanMaps
