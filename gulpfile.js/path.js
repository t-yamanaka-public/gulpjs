
const PATH = {
  SRC: 'src/',
  DIST: 'dist/',
  ASSETS: {
    SRC: 'src/assets/',
    DIST: 'dist/assets/'
    // WPDIST: 'wp/wp-content/themes/site_odakyu_engineering/assets/'
  }
}
exports.PATH = PATH
