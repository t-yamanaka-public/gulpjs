
const { PATH } = require('./path')
const { src, dest } = require('gulp')
const plumber = require('gulp-plumber')
const browserify = require('browserify')
const babelify = require('babelify')
const through2 = require('through2')
const browser = require('browser-sync')
const uglify = require('gulp-uglify')

function babel (done) {
  return src([
    `${PATH.ASSETS.SRC}js/**/[^_]*.js`,
    // `!${PATH.ASSETS.SRC}js/jquery-1.12.4.min.js`
  ])
    .pipe(plumber())
    .pipe(through2.obj((file, encode, callback) => {
      browserify(file.path)
        .transform(
          babelify,
          {
            presets: [
              ['@babel/preset-env', {
                targets: {
                  ie: '11'
                },
                useBuiltIns: 'usage',
                corejs: 3,
                debug: false
              }]
            ],
            plugins: ['@babel/plugin-proposal-class-properties']
          }
        )
        .bundle((err, res) => {
          if (err) {
            return callback(err)
          }
          file.contents = res
          callback(null, file)
        })
    }))
    .pipe(uglify())
    .pipe(dest(`${PATH.ASSETS.DIST}js`))
    .pipe(browser.reload({ stream: true }))
}

exports.babel = babel
