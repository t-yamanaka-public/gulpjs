
const { PATH } = require('./path')
const { src, dest } = require('gulp')
const postcss = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const plumber = require('gulp-plumber')
const sass = require('gulp-sass')
const sassGlob = require('gulp-sass-glob')
const purgecss = require('gulp-purgecss')
const cleancss = require('gulp-clean-css')
const rename = require('gulp-rename')
const browser = require('browser-sync').get('myserver')

// styles(sass)
function styles () {
  return src(`${PATH.ASSETS.SRC}**/*.scss`, { sourcemaps: true })
    .pipe(plumber())
    .pipe(sassGlob())
    .pipe(sass({
      includePaths: ['./node_modules'],
      outputStyle: 'compressed',
      // outputStyle: 'expanded',
      sourceMap: true
    }).on('error', sass.logError))
    .pipe(rename({
      extname: '.css'
    }))
    .pipe(postcss([
      autoprefixer({
        grid: 'autoplace',
        cascade: false
      })
    ]))
    .pipe(dest(PATH.ASSETS.DIST, { sourcemaps: './maps' }))
    .pipe(browser.stream())
}
exports.styles = styles

// minify
function minCss () {
  return src(`${PATH.ASSETS.DIST}**/*.css`)
    .pipe(plumber())
    .pipe(purgecss({
      content: [`${PATH.DIST}**/*.html`],
      safelist: {
        standard: [
          /^is-/,
          /^js-/,
          /^aria-/,
          /^anim/,
          /^scroll-/,
          /^android/,
          /^jz-/,
          /^select-/,
          /--inversion$/,
          'ie',
          'edge',
          'firefox',
          'safari',
          'less14',
          'windows',
          'touch-device',
          'not-touch-device',
          'disabled'
        ]
      }
    }))
    .pipe(cleancss())
    .pipe(dest(PATH.ASSETS.DIST))
}
exports.minCss = minCss
