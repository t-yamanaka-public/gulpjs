
const { PATH } = require('./path')
const pug2 = require('pug')
// const url = require('url') // Legacy
const { URL } = require('url')
const path = require('path')
const fileExists = require('file-exists')

// pugMiddleWare
function pugMiddleWare (req, res, next) {
  // const requestPath = url.parse(req.url).pathname // Legacy
  const requestPath = new URL(req.url, 'relative:///').pathname

  if (!requestPath.match(/(\/|\.html)$/)) {
    return next()
  }
  const htmlPath = path.parse(requestPath).ext === '' ? `${requestPath}index.html` : requestPath
  if (fileExists.sync(path.join(PATH.DIST, htmlPath))) {
    const pugPath = path.join(PATH.SRC, htmlPath.replace('.html', '.pug'))

    if (!fileExists.sync(pugPath)) {
      console.info(`Pug file not found. ${pugPath}`)
    }
    const content = pug2.renderFile(pugPath, {
      basedir: PATH.SRC,
      pretty: false
    })

    res.end(Buffer.from(content))
  } else {
    console.log('Html file not found.')
  }
}
exports.pugMiddleWare = pugMiddleWare
