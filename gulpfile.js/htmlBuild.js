
const { PATH } = require('./path')
const { src, dest } = require('gulp')
const plumber = require('gulp-plumber')
const pug = require('gulp-pug')
const htmlbeautify = require('gulp-html-beautify')

// htmlBuild
function htmlBuild () {
  return src([`${PATH.SRC}**/*.pug`, `!${PATH.SRC}**/_*.pug`])
    .pipe(plumber())
    .pipe(pug({
      // pretty: true,
      basedir: PATH.SRC
    }))
    .pipe(htmlbeautify({
      content_unformatted: ['pre'],
      eol: '\n',
      extra_liners: [],
      indent_char: ' ',
      indent_inner_html: true,
      indent_size: 2,
      inline: ['span', 'br'],
      max_preserve_newlines: 3,
      preserve_newlines: true,
      unformatted: ['script']
    }))
    .pipe(dest(PATH.DIST))
}
exports.htmlBuild = htmlBuild
