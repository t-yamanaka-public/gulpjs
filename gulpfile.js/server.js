
const { PATH } = require('./path')
const { pugMiddleWare } = require('./pugMiddleWare')
const browser = require('browser-sync').get('myserver')

function server (done) {
  browser.init({
    server: {
      baseDir: PATH.DIST,
      middleware: [pugMiddleWare]
    },
    // https: {
    //   key: '/Applications/MAMP/conf/apache/keys/_wildcard.localhost.com+1-key.pem',
    //   cert: '/Applications/MAMP/conf/apache/keys/_wildcard.localhost.com+1.pem'
    // },
    // proxy: {
    //   target: 'local.〇〇.com'
    // },
    open: false,
    directory: true,
    // startPath: '/foo/',
    scrollProportionally: false,
    ghostMode: false
  })
  done()
}
exports.server = server
