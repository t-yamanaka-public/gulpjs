
'use strict'

const { series, parallel, watch } = require('gulp')
const browser = require('browser-sync').create('myserver')
const { PATH } = require('./path')
const { server } = require('./server')
const { clean, cleanMaps } = require('./clean')
exports.cleanMaps = cleanMaps
exports.clean = clean
const { htmlBuild } = require('./htmlBuild')
exports.htmlBuild = htmlBuild
const { styles, minCss } = require('./styles')
exports.styles = styles
exports.minCss = minCss
const { imgMin } = require('./imgMin')
exports.imgMin = imgMin
const { svgSprite, svgSpriteAttr } = require('./svgSprite')
exports.svgSprite = svgSprite
exports.svgSpriteAttr = svgSpriteAttr
const { copy, copyAssets, copyImg } = require('./copy')
exports.copy = copy
exports.copyAssets = copyAssets
exports.copyImg = copyImg
const { babel } = require('./babel')
exports.babel = babel

// reload
function reload () {
  setTimeout(() => {
    browser.reload()
  }, 200)
}

// watch
function watchFiles () {
  watch(`${PATH.SRC}**/*.pug`).on('change', reload)
  watch([`${PATH.SRC}**/*.pug`, `!${PATH.SRC}**/_*.pug`]).on('add', htmlBuild)
  // watch(`${PATH.SRC}**/*.pug`).on('change', series(htmlBuild, reload))
  watch(`${PATH.ASSETS.SRC}**/*.scss`).on('change', styles)
  // watch([
  //   `${PATH.SRC}**/*.+(svg|png|jpg|gif|pdf|ico)`,
  //   `!${PATH.ASSETS.SRC}img/svg-sprite/*.svg`,
  //   `!${PATH.ASSETS.SRC}img/svg-sprite-attr/*.svg`
  // ]).on('all', series(copyImg, reload))
  watch([
    `${PATH.SRC}**/*.+(svg|png|jpg|gif|pdf|ico)`,
    `!${PATH.ASSETS.SRC}img/svg-sprite/*.svg`,
    `!${PATH.ASSETS.SRC}img/vg-sprite-attr/*.svg`
  ]).on('all', series(imgMin, reload))
  watch(`${PATH.ASSETS.SRC}img/svg-sprite/*.svg`).on('all', series(svgSprite, reload))
  watch(`${PATH.ASSETS.SRC}img/svg-sprite-attr/*.svg`).on('all', series(svgSpriteAttr, reload))
  watch([`${PATH.SRC}**/*.{html,php,tsv,csv,xml,webmanifest}`]).on('all', series(copy, reload))
  watch([`${PATH.SRC}.htaccess*`, `${PATH.SRC}.htpasswd*`]).on('all', series(copy, reload))
  watch([`${PATH.ASSETS.SRC}**/*.{css,otf,ttf,eot,woff,woff2,pdf,json,jsonp,mp4,zip}`]).on('all', series(copyAssets, reload))
  // watch([`${PATH.ASSETS.SRC}**/bg_hero-top_*.jpg`]).on('all', series(copyImg, reload))
  watch([`${PATH.ASSETS.SRC}**/*.js`]).on('change', series(babel, reload))
}
exports.default = parallel(server, watchFiles)

// build
exports.build = series(clean, parallel(htmlBuild, styles, imgMin, svgSprite, svgSpriteAttr, babel, copy, copyAssets, copyImg), minCss, cleanMaps)

// build（※画像非圧縮）
// exports.build = series(clean, parallel(htmlBuild, styles, svgSprite, svgSpriteAttr, babel, copy, copyAssets, copyImg), cleanMaps)
