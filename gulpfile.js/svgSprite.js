
const { PATH } = require('./path')
const { src, dest } = require('gulp')
const svgsprite = require('gulp-svg-sprite')
const cheerio = require('gulp-cheerio')

// svg sprite
function svgSprite () {
  return src(`${PATH.ASSETS.SRC}img/svg-sprite/*.svg`)
    .pipe(svgsprite({
      mode: {
        symbol: {
          dest: './',
          sprite: 'svg-icons.svg'
        }
      },
      shape: {
        transform: [
          {
            svgo: {
              plugins: [
                { removeTitle: true },
                { removeStyleElement: true },
                { removeAttrs: { attrs: '(fill|stroke)' } },
                { removeXMLNS: true },
                { removeDimensions: true }
              ]
            }
          }
        ]
      },
      svg: {
        xmlDeclaration: false
      }
    }))
    .pipe(cheerio({
      run: function ($) {
        $('defs').remove()
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(dest(`${PATH.ASSETS.DIST}img/svg-sprite`))
    // .pipe(dest(`${PATH.ASSETS.WPDIST}img/svg-sprite`))
}
exports.svgSprite = svgSprite

// svg sprite(not removeAttrs fill and stroke)
function svgSpriteAttr () {
  return src(`${PATH.ASSETS.SRC}img/svg-sprite-attr/*.svg`)
    .pipe(svgsprite({
      mode: {
        symbol: {
          dest: './',
          sprite: 'svg-icons-attr.svg'
        }
      },
      shape: {
        transform: [
          {
            svgo: {
              plugins: [
                { removeTitle: true },
                { removeStyleElement: true },
                // { removeAttrs: { attrs: '(fill|stroke)' } },
                { removeXMLNS: true },
                { removeDimensions: true }
              ]
            }
          }
        ]
      },
      svg: {
        xmlDeclaration: false
      }
    }))
    .pipe(cheerio({
      run: function ($) {
        $('defs').remove()
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(dest(`${PATH.ASSETS.DIST}img/svg-sprite-attr`))
    // .pipe(dest(`${PATH.ASSETS.WPDIST}img/svg-sprite-attr`))
}
exports.svgSpriteAttr = svgSpriteAttr
