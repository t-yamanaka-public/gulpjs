
const { PATH } = require('./path')
const { src, dest } = require('gulp')
const imagemin = require('gulp-imagemin')
const mozjpeg = require('imagemin-mozjpeg')
const pngquant = require('imagemin-pngquant')
const changed = require('gulp-changed')

// imgMin
function imgMin () {
  return src([
    `${PATH.SRC}**/*.+(png|jpg|gif|svg|pdf|ico)`,
    `!${PATH.ASSETS.SRC}**/svg-sprite/*.svg`,
    `!${PATH.ASSETS.SRC}**/svg-sprite-attr/*.svg`
    // `!${PATH.ASSETS.SRC}**/bg_hero-top_*.jpg` 圧縮しない画像
  ], {
    // since: lastRun(imgMin)
  })
    .pipe(changed(PATH.DIST))
    .pipe(imagemin([
      pngquant({
        quality: [0.7, 0.8],
        speed: 3,
        floyd: 0
      }),
      mozjpeg({
        quality: 80,
        progressive: true
      }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { removeMetadata: false },
          { removeUnknownsAndDefaults: false },
          { convertShapeToPath: false },
          { collapseGroups: false },
          { removeDimensions: false },
          { cleanupIDs: false }
        ]
      }),
      imagemin.optipng(),
      imagemin.gifsicle()
    ]))
    .pipe(dest(PATH.DIST))
    // .pipe(dest(PATH.ASSETS.WPDIST))
}
exports.imgMin = imgMin
