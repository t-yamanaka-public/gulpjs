<br>

# 開発の流れ
<br>

## 1. モジュールのインストール（初回のみ）

- node: 14.0.0　
- npm: 6.14.0

```
npm i
```
<br>

## 2. ファイルの初期化（初回のみ）

```
npm run build
```
<br>

## 3. プロジェクトの実行

```
npm start
```
<br>

## 4. 開発

- JSで動的に追加するclass名にはプレフィックス `.is-` を付けるか、<br>
scssファイル内のセレクタを下記コメントで囲んでください<br>
（※PurgeCSSで不要セレクタとして削除されないようにするため）
```
/*! purgecss start ignore */
（動的に追加するセレクタ）
/*! purgecss end ignore */
```

- 画像が書き出されない場合：`npm run img` を実行

- "Html file not found."と出る場合：`npm run html` を実行

- 整形済みのローカルHTMLをブラウザ上で確認したい場合：<br>
`npm run html`⇒ gulpfile.js/server.jsの`middleware: [pugMiddleWare]`をコメントアウト⇒`npm start`
<br>

## 5. ビルド（不要ファイルの削除/htmlのコンパイル/画像の圧縮など）

```
npm run build
```
<br>

## 6. Gitコミット
<br>

## 7. FTPアップ/納品など
<br>
