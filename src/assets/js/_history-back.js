
/* Import
---------------------- */

/* Variable
---------------------- */
const historyBackElements = document.querySelectorAll('.js-history-back')

/* Function
---------------------- */
const historyBack = () => {
  historyBackElements.forEach((element, value) => {
    element.addEventListener('click', function (event) {
      event.preventDefault()
      history.back()
    })
  })
}

/* Export
---------------------- */
export { historyBack }
