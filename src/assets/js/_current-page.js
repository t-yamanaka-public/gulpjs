
/* Import
---------------------- */

/* Variable
---------------------- */
const urlPath = location.pathname.split('/')[1]

/* Class
---------------------- */
class CurrentPage {
  constructor (targets) {
    this.targets = document.querySelectorAll(targets)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  // init () {
  // }

  // メイン関数
  main (event) {
    // const target = event.currentTarget
    this.targets.forEach((element, index) => {
      const hrefPath = element.getAttribute('href').split('/')[1]
      const hrefTarget = element.getAttribute('target')

      /* target = "_blank"のリンク以外で */
      if (hrefTarget !== '_blank') {
        if (urlPath === hrefPath) {
          element.setAttribute('aria-current', 'page')
        }
      }
    })
  }

  // イベントの登録
  addListener () {
    document.addEventListener('DOMContentLoaded', this.main)
  }

  // イベントの解除
  // removeListener () {
  // }

  // 実行
  run () {
    this.prepare()
    // this.init()
    this.addListener()
  }
}

// Normal
const fnCurrentPage1 = new CurrentPage('.l-gnav__target')
const fnCurrentPage2 = new CurrentPage('.l-header__subnav .subnav__target')

export { fnCurrentPage1, fnCurrentPage2 }
