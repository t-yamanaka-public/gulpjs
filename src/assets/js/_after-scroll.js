
/* Import
---------------------- */
import { accordion2 } from './_accordion'
import { fnDrawer } from './_drawer'

/* Variable
---------------------- */
const targetClass1 = '.p-megamenu-a__target'
const targetClass2 = '.l-drawer .gnav__target'
const targetClass3 = '.l-drawer__subnav .subnav__target'

/* Class
---------------------- */
class AfterScroll {
  constructor (target) {
    this.target = target
    this.targets = document.querySelectorAll(target)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    // 初期設定
  }

  // メイン関数
  main (event) {
    // スクロールが終わってから処理を実行する
    let timer
    window.addEventListener('scroll', () => {
      clearTimeout(timer)
      timer = setTimeout(() => {
        switch (this.target) {
          case targetClass1:
            accordion2.closeAll()
            break
          case targetClass2:
            fnDrawer.close()
            break
          case targetClass3:
            fnDrawer.close()
            break
          default:
            return false
        }
      }, 200)
    }, { passive: true })
  }

  // イベントの登録
  addListener () {
    this.targets.forEach((element, index) => {
      element.addEventListener('click', this.main, false)
    })
  }

  // イベントの解除
  // removeListener () {
  //   this.tabs.forEach((element, index) => {
  //     element.removeEventListener('click', this.main, false)
  //   })
  // }

  // 実行
  run () {
    this.prepare()
    this.init()
    this.addListener()
  }
}

const afterScrollMega = new AfterScroll(targetClass1)
const afterScrollDrawer = new AfterScroll(targetClass2)
const afterScrollDrawerSub = new AfterScroll(targetClass3)

/* Usage
---------------------- */

export { afterScrollMega, afterScrollDrawer, afterScrollDrawerSub }
