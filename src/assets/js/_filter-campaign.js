
/* Import
---------------------- */
import { accordionSP } from './_accordion'

/* Variable
---------------------- */
const filterButtons = document.querySelectorAll('.p-filter-button__panel button')
const campaignItems = document.querySelectorAll('.cpn')
const campaignItemWrap = document.querySelector('.cpn-wrap')
const pickupSelectClass = 'select-pickup'
const filterTab = document.querySelector('.p-filter-button .c-acc-sp__tab')
const filterPanel = document.querySelector('.p-filter-button .c-acc-sp__panel')
const filterCurrent = document.querySelector('.p-filter-button__current')
const pickupItems = document.querySelectorAll('.cpn-pickup')

/* Function
---------------------- */
const filterCampaign = {

  run: function () {
    // 導入事例一覧のみで実行する
    if (!document.body.classList.contains('case-index')) return false
    if (!filterTab) return false
    this.addListeners()
    this.filter()
    this.order()
  },

  filter: function (event) {
    if (!location.hash) {
      // URLハッシュがない時はピックアップを表示する
      history.replaceState({}, '', '#cpn-pickup')
    }
    if (event) {
      // filterButtonsの何番目をクリックしたかによってURLハッシュを付加する
      history.replaceState({}, '', this.index === 0 ? '#cpn-pickup' : `#cpn-${this.index}`)
    }
    // URLハッシュの取得
    const hash = location.hash.split('#')[1]
    const hashNum = hash.split('-')[1]
    // 絞り込みボタンのカレント制御
    filterButtons.forEach((element, index) => {
      /* -- クリックした時 -- */
      if (event) {
        // クリックしたボタンのカレント表示
        element.setAttribute('aria-selected', 'false')
        event.target.setAttribute('aria-selected', 'true')
        /* -- ピックアップをクリックしたとき -- */
        if (hashNum === 'pickup') {
          campaignItemWrap.classList.add(pickupSelectClass)
        } else {
          campaignItemWrap.classList.remove(pickupSelectClass)
        }
        // カレント表示エリアの制御（SPのみ）
        const clickedText = event.target.children[0].innerText
        document.querySelector('.p-filter-button__current .c-button-a__text').innerText = clickedText
        filterCurrent.setAttribute('aria-hidden', 'false')
        accordionSP.close(filterTab, filterPanel)
      /* -- loadした時 -- */
      } else {
        if (location.hash) {
          element.setAttribute('aria-selected', 'false')
          /* -- URLハッシュとfilterButtonsのindexが同じとき -- */
          if (String(index) === hashNum) {
            element.setAttribute('aria-selected', 'true')
            campaignItemWrap.classList.remove(pickupSelectClass)
          /* -- filterButtonsのindexが 0かつURLハッシュが「pickup」のとき -- */
          } else if (index === 0 && hashNum === 'pickup') {
            element.setAttribute('aria-selected', 'true')
            campaignItemWrap.classList.add(pickupSelectClass)
          }
          // 選択中のテキストをカレント表示エリアに入れる（SPのみ）
          if (element.getAttribute('aria-selected') === 'true') {
            document.querySelector('.p-filter-button__current .c-button-a__text').innerText = element.textContent
          }
        }
      }
    })
    // 絞り込み（#cpn-allの時は絞り込まない）
    if (hashNum !== 'all') {
      campaignItems.forEach((element, index) => {
        // 全て非表示
        element.setAttribute('aria-hidden', 'true')
      })
      const items = document.querySelectorAll(`.cpn-${hashNum}`)
      const itemArr = Array.prototype.slice.call(items, 0)
      itemArr.forEach((element, index) => {
        // ハッシュと同じ番号の.cpn-Nを表示する
        element.setAttribute('aria-hidden', 'false')
      })
    }
  },

  order: function () {
    // order-NクラスのN（数字）を取得する
    const regex = /order-\d+/
    pickupItems.forEach((element, index) => {
      element.classList.forEach((classItem, index) => {
        const orderMatch = regex.test(classItem)
        /* -- order-Nにマッチしたとき -- */
        if (orderMatch) {
          const orderNum = Number(classItem.match(/\d+/g)[0]) // order-Nクラスの数字
          // order-Nクラスの要素を降順に並べる
          element.style.order = orderNum
        }
      })
    })
  },

  current: function () {
    const expanded = this.getAttribute('aria-expanded')
    /* -- パネルが開いているとき -- */
    if (expanded === 'true') {
      // 表示
      filterCurrent.setAttribute('aria-hidden', 'false')
    /* -- パネルが閉じているとき -- */
    } else {
      // 非表示
      filterCurrent.setAttribute('aria-hidden', 'true')
    }
  },

  addListeners: function () {
    filterButtons.forEach((element, index) => {
      element.addEventListener('click', {
        index: index,
        handleEvent: this.filter
      }, false)
    })
    filterTab.addEventListener('click', this.current, false)
  }
}

export { filterCampaign }
