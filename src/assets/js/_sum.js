
/* Import
---------------------- */

/* Variable
---------------------- */

/* Class
---------------------- */
class Sum {
  constructor (item, displayElm, trigger) {
    this.item = item
    this.displayElms = document.querySelectorAll(displayElm)
    this.triggers = document.querySelectorAll(trigger)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    this.main()
  }

  // メイン関数
  main (event) {
    // const target = event.currentTarget
    this.items = document.querySelectorAll(this.item)
    let total = 0
    this.items.forEach((element, index) => {
      total = index + 1
    })
    this.displayElms.forEach((element, index) => {
      element.innerHTML = total
    })
    // if (total === 0) {
    //   this.displayElms.forEach((element) => {
    //     element.closest('.p-list-thumb__shoulder').classList.add('is-nodata')
    //   })
    // }
  }

  // イベントの登録
  addListener () {
    if (this.triggers.length) {
      this.triggers.forEach((element, index) => {
        element.addEventListener('click', this.main, false)
      })
    }
  }

  // 実行
  run () {
    this.prepare()
    this.init()
    this.addListener()
  }
}

/*
const fnSum = new Sum('合計したい要素', '合計値の出力先', 'clickで発火させる要素')
*/
const fnSum1 = new Sum('.p-thumb-a', '.p-list-thumb__shoulder span', null)

export { fnSum1 }
