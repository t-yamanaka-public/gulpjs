
/* Import
---------------------- */

/* Usage
----------------------
import { scrollControl } from './_scroll-control'
...
scrollControl.fixed() // スクロールを止める
...
scrollControl.release() // スクロール固定を解除する
*/

/* Variable
---------------------- */
const html = document.querySelector('html')

/* Class
---------------------- */
class ScrollControl {
  constructor (fixedClass) {
    this.fixedClass = fixedClass
  }

  // スクロールを固定する
  fixed () {
    html.classList.add(this.fixedClass)
    // document.addEventListener('touchmove', this.disableScroll, { passive: false })
  }

  // スクロール固定を解除する
  release () {
    html.classList.remove(this.fixedClass)
    // document.removeEventListener('touchmove', this.disableScroll, { passive: false })
  }

  // touchmoveでのスクロールを無効にする
  disableScroll () {
    event.preventDefault()
  }
}

const scrollControl = new ScrollControl('scroll-fixed')

export { scrollControl }




/* Import
---------------------- */

/* Variable
---------------------- */
// const html = document.querySelector('html')
//
// /* Function
// ---------------------- */
// const disableScroll = (event) => {
//   event.preventDefault()
// }
//
// const stopScroll = (fixedClass) => {
//   html.classList.add(fixedClass)
//   document.addEventListener('touchmove', disableScroll, { passive: false })
// }
//
// const releaseScroll = (fixedClass) => {
//   html.classList.remove(fixedClass)
//   document.removeEventListener('touchmove', disableScroll, { passive: false })
// }
//
// export { stopScroll, releaseScroll }
