
/* Import
---------------------- */

/* Usage
---------------------- */
/*
.js-append // コンテナ要素
  .js-append__pc // PC時のappend先
  .js-append__sp // SP時のappend先
*/

/* Variable
---------------------- */
const BREAKP = 767
const mediaQuerySP = window.matchMedia(`(max-width: ${BREAKP}px)`)

/* Class
---------------------- */
class Appends {
  constructor (container, destPC, destSP) {
    this.container = container
    this.destPC = destPC
    this.destSP = destSP
  }

  // 準備
  prepare () {
    this.mainSP = this.mainSP.bind(this) // this.main()中のthisをClassのthisに固定する
    this.mainPC = this.mainPC.bind(this)
    this.containers = document.querySelectorAll(this.container)
    this.destsPC = document.querySelectorAll(this.destPC)
    this.destsSP = document.querySelectorAll(this.destSP)
  }

  // 初期化
  init () {
  }

  // メイン関数（for SP）
  mainSP (event) {
    this.destsPC.forEach((destPC) => {
      if (!destPC.children[0]) return false
      destPC.closest(this.container).querySelector(this.destSP).appendChild(destPC.children[0])
    })
  }

  // メイン関数（for PC）
  mainPC (event) {
    this.destsSP.forEach((destSP) => {
      if (!destSP.children[0]) return false
      destSP.closest(this.container).querySelector(this.destPC).appendChild(destSP.children[0])
      // console.log(destSP)
    })
  }

  // イベントの登録
  addListeners () {
    // MatchMedia
    const listener = (event) => {
      if (event.matches) {
        // SP
        this.mainSP()
      } else {
        // PC
        this.mainPC()
      }
    }
    mediaQuerySP.addListener(listener)
    listener(mediaQuerySP)
  }

  // 実行
  run () {
    if (!this.container) return false
    this.prepare()
    // this.init()
    this.addListeners()
  }
}

// const fnAppends = new Appends('コンテナ要素', 'appendする要素の親要素（PC時）', 'appendする要素の親要素（SP時）')
const fnAppends = new Appends('.js-append', '.js-append__pc', '.js-append__sp')

/* Usage
---------------------- */

export { fnAppends }
