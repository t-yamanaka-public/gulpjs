
/* Variable
---------------------- */
const pagetop = document.getElementById('pagetop')
const wrapper = document.getElementById('wrapper')
const footer = document.getElementById('footer-wrap')
const DISPLAY_POSITION = 500 // 表示される位置
// const BUTTON_HEIGHT = pagetop.clientHeight // ボタンの高さ
const BUTTON_MARGIN_BOTTOM = 15 // ボタンの下マージン
const breadcrumbsSP = document.getElementById('breadcrumbs-sp')
let breadcrumbsSPH

/* Function
---------------------- */
const fnPageTop = {

  run: function () {
    this.addListener()
    this.func()
  },

  func: function () {
    const winH = window.innerHeight
    const scrollTop = document.documentElement.scrollTop
    const footerH = footer.clientHeight
    const pageH = wrapper.clientHeight
    if (breadcrumbsSP) {
      breadcrumbsSPH = breadcrumbsSP.clientHeight
    } else {
      breadcrumbsSPH = 0
    }

    /* scrollTopが500以上のとき */
    if (scrollTop >= DISPLAY_POSITION) {
      // pagetopを表示
      pagetop.setAttribute('aria-hidden', 'false')

      // pagetopを固定する位置の制御
      // if (scrollTop >= (pageH - winH - footerH - breadcrumbsSPH) + (BUTTON_HEIGHT / 2) + BUTTON_MARGIN_BOTTOM) {}
      if (scrollTop >= pageH - winH - footerH - breadcrumbsSPH) {
        pagetop.classList.remove('is-fixed')
      } else {
        pagetop.classList.add('is-fixed')
      }

    /* scrollTopが500未満のとき */
    } else {
      // pagetopを非表示
      pagetop.setAttribute('aria-hidden', 'true')
    }
  },

  addListener: function () {
    window.addEventListener('scroll', this.func, false)
  }
}

export { fnPageTop }
