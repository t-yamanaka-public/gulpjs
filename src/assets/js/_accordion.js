
/* Import
---------------------- */
import { overlay3 } from './_overlay'

/* Usage
---------------------- */
/*
.c-acc.c-acc-●
  .c-acc__tab タブ
    .c-acc__child
      ...
  .c-acc__panel パネル
    .c-acc__child
      ...
*/

/* Variable
---------------------- */
// let listenerFlag
const accParentElm = '.c-acc'
const tabElm = '.c-acc__tab'
const panelElm = '.c-acc__panel'
const defaultOpenClass = '.c-acc-open'
const tabs = document.querySelectorAll(tabElm)
const panels = document.querySelectorAll(panelElm)

/* Class
---------------------- */
class Accordion {
  constructor (tab, panel, toggle, overlay, fade) {
    this.tabs = document.querySelectorAll(tab)
    this.panels = document.querySelectorAll(panel)
    this.toggle = toggle
    this.overlay = overlay
    this.fade = fade
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    // タブの初期設定
    tabs.forEach((element, index) => {
      index++
      /* -- デフォルトオープンの要素 -- */
      if (element.closest(defaultOpenClass)) {
        element.setAttribute('aria-expanded', 'true')
      /* -- デフォルトクローズの要素 -- */
      } else {
        element.setAttribute('aria-expanded', 'false')
      }
      element.setAttribute('aria-controls', `panel-${index}`)
    })
    // パネルの初期設定
    panels.forEach((element, index) => {
      index++
      element.id = `panel-${index}`
    })
    this.panels.forEach((element, index) => {
      const panelChildH = element.firstElementChild.clientHeight
      /* -- デフォルトオープンの要素 -- */
      if (element.closest(defaultOpenClass)) {
        element.setAttribute('aria-hidden', 'false')
        element.style.height = `${panelChildH}px`
      /* -- デフォルトクローズの要素 -- */
      } else {
        element.setAttribute('aria-hidden', 'true')
        element.style.height = 0
      }
    })
  }

  // パネルを開く処理
  open (target, thisPanel, panelChildH, fade) {
    /* -- fadeのとき -- */
    if (fade) {
      thisPanel.style.height = 'auto'
    } else {
      thisPanel.style.height = `${panelChildH}px`
    }
    thisPanel.setAttribute('aria-hidden', 'false')
    target.setAttribute('aria-expanded', 'true')
    // オーバーレイを表示
    if (this.overlay) {
      overlay3.show()
    }
  }

  // パネルを閉じる処理
  close (target, thisPanel, fade) {
    thisPanel.setAttribute('aria-hidden', 'true')
    target.setAttribute('aria-expanded', 'false')
    /* -- fadeのとき -- */
    if (fade) {
      setTimeout(() => {
        thisPanel.style.height = 0
      }, 200)
    } else {
      thisPanel.style.height = 0
    }
    if (this.overlay) {
      overlay3.hide()
    }
  }

  // パネルを閉じる処理（all）
  closeAll (target, thisPanel) {
    this.tabs.forEach((element, index) => {
      element.setAttribute('aria-expanded', 'false')
    })
    this.panels.forEach((element, index) => {
      element.setAttribute('aria-hidden', 'true')
      element.style.height = 0
    })
    if (this.overlay) {
      overlay3.hide()
    }
  }

  // SPのtabとpanelをリセット
  resetSP () {
    this.tabs.forEach((element, index) => {
      element.setAttribute('aria-expanded', 'true')
    })
    this.panels.forEach((element, index) => {
      element.setAttribute('aria-hidden', 'false')
      element.style.height = 'auto'
    })
  }

  // PCのtabとpanelをリセット
  resetPC () {
    this.tabs.forEach((element, index) => {
      element.setAttribute('aria-expanded', 'false')
    })
    this.panels.forEach((element, index) => {
      element.setAttribute('aria-hidden', 'true')
      element.style.height = 0
    })
  }

  // メイン関数
  main (event) {
    const target = event.currentTarget
    const expanded = target.getAttribute('aria-expanded')
    const thisPanel = target.closest(accParentElm).querySelector(panelElm)
    const panelChildH = thisPanel.firstElementChild.clientHeight

    /* -- パネルが閉じているとき -- */
    if (expanded === 'false') {
      if (this.toggle) {
        this.closeAll()
      }
      this.open(target, thisPanel, panelChildH, this.fade)
    /* -- パネルが開いているとき -- */
    } else {
      this.close(target, thisPanel, this.fade)
    }
  }

  // イベントの登録
  addListener () {
    // if (listenerFlag) return false //イベントの登録を1回だけにする
    this.tabs.forEach((element, index) => {
      element.addEventListener('click', this.main, false)
    })
    /* -- overlayがtrueかつ#overlay-3が存在するとき -- */
    if (this.overlay && overlay3.overlayElm) {
      overlay3.overlayElm.addEventListener('click', () => {
        this.closeAll()
      })
    }
    // listenerFlag = true
  }

  // イベントの解除
  removeListener () {
    this.tabs.forEach((element, index) => {
      element.removeEventListener('click', this.main, false)
    })
  }

  // 実行
  run () {
    this.prepare()
    this.init()
    this.addListener()
  }
}

const accordion1 = new Accordion( // footerメニュー（SP時）
  `.c-acc-1 ${tabElm}`,
  `.c-acc-1 ${panelElm}`,
  false, // toggle
  false, // overlay
  false // fade
)

const accordion2 = new Accordion( // トップFAQ
  `.c-acc-2 ${tabElm}`,
  `.c-acc-2 ${panelElm}`,
  false, // toggle
  false, // overlay
  false // fade
)

const accordion3 = new Accordion( // 絞り込み検索パネル
  `.c-acc-3 ${tabElm}`,
  `.c-acc-3 ${panelElm}`,
  false, // toggle
  true, // overlay
  false // fade
)

const accordion4 = new Accordion( // FAQページ
  `.c-acc-4 > ${tabElm}`,
  `.c-acc-4 > ${panelElm}`,
  false, // toggle
  false, // overlay
  true // fade
)

export { accordion1, accordion2, accordion3, accordion4 }
