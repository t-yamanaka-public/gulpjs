
/* Import
---------------------- */

/* Variable
---------------------- */

/* Class
---------------------- */
class Overlay {
  constructor (overlay) {
    this.overlayElm = document.getElementById(overlay)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // オーバーレイを表示
  show () {
    this.overlayElm.setAttribute('aria-hidden', 'false')
  }

  // オーバーレイを非表示
  hide () {
    this.overlayElm.setAttribute('aria-hidden', 'true')
  }

  // メイン関数
  main () {
    const overlayHidden = this.overlayElm.getAttribute('aria-hidden')

    /* -- オーバーレイが表示しているとき -- */
    if (overlayHidden === 'false') {
      this.hide()
    }
  }

  // イベントの登録
  addListener () {
    this.overlayElm.addEventListener('click', () => {
      this.main()
    })
  }

  // 実行
  run () {
    this.prepare()
    this.addListener()
  }
}

const overlay1 = new Overlay('overlay-1')
const overlay2 = new Overlay('overlay-2')
const overlay3 = new Overlay('overlay-3')

/* Usage
----------------------
import { overlay1, overlay2 } from './_overlay'
...
overlay1.show()
...
overlay1.hide()
...
overlay1.overlayElm.addEventListener('click', () => {
  overlay1.hide()
  this.closeAll() // パネルを閉じるなどの処理
})
*/

export { overlay1, overlay2, overlay3 }
