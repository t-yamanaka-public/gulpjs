
/* Import
---------------------- */
import { gsap } from 'gsap'
// import { ScrollTrigger } from 'gsap/ScrollTrigger'
import { ScrollTrigger } from './modules/_ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)

/* Variable
---------------------- */
const animHeading = document.querySelectorAll('.anim-heading-a')
const animHeadings = Array.prototype.slice.call(animHeading, 0)
const animFadein = document.querySelectorAll('.c-anim-fadein')
const animFadeins = Array.prototype.slice.call(animFadein, 0)
const vMove = document.querySelectorAll('.c-anim-v-move')
const vMoves = Array.prototype.slice.call(vMove, 0)
const hMove = document.querySelectorAll('.c-anim-h-move')
const hMoves = Array.prototype.slice.call(hMove, 0)
const heroTitle = document.getElementById('hero-title')

/* Function
---------------------- */

/* Execution
---------------------- */
// fnScrollTrigger
const fnScrollTrigger = () => {
  // Hero title
  gsap.to(heroTitle, {
    scrollTrigger: {
      once: true,
      toggleClass: 'active',
      trigger: heroTitle,
      start: 'center bottom'
      // markers: true
    }
  })
  // animHeadings
  animHeadings.forEach(function (element) {
    gsap.to(element, {
      scrollTrigger: {
        once: true,
        toggleClass: 'active',
        trigger: element,
        start: 'top bottom-=30%'
        // markers: true
      }
    })
  })
  // animFadein
  animFadeins.forEach(function (element) {
    gsap.to(element, {
      scrollTrigger: {
        once: true,
        toggleClass: 'active',
        trigger: element,
        start: 'top bottom-=30%'
        // markers: true
      }
    })
  })
  // Vertical movement
  vMoves.forEach(function (element) {
    gsap.to(element, {
      scrollTrigger: {
        once: true,
        toggleClass: 'active',
        trigger: element,
        start: 'top bottom-=45%'
        // markers: true
      }
    })
  })
  // Horizontal movement
  hMoves.forEach(function (element) {
    gsap.to(element, {
      scrollTrigger: {
        once: true,
        toggleClass: 'active',
        trigger: element,
        start: 'top bottom-=45%'
        // markers: true
      }
    })
  })
}

/* Export
---------------------- */
export { fnScrollTrigger }
