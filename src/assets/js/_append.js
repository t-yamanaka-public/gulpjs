
/* Import
---------------------- */

/* Usage
---------------------- */
/*
#parent // 移動したい要素の親要素
  #target // ** 移動したい要素 **
...
#dest // 移動先の要素
*/

/* Variable
---------------------- */

/* Class
---------------------- */
class Append {
  constructor (target, dest) {
    this.target = document.getElementById(target)
    this.dest = document.getElementById(dest)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
  }

  // メイン関数
  main (event) {
    this.dest.appendChild(this.target)
  }

  // イベントの登録
  addListener () {
    // document.addEventListener('DOMContentLoaded', this.main, false)
    this.main()
  }

  // 実行
  run () {
    if (!this.target) return false
    this.prepare()
    // this.init()
    this.addListener()
  }
}

// const fnAppend = new Append('移動したい要素のid', '移動先のid') // SPで実行
// const fnAppendR = new Append('移動したい要素のid', '移動したい要素の親のid') // PCで実行
const fnAppend1 = new Append('panel-filter', 'append-panel-filter')
const fnAppend1R = new Append('panel-filter', 'panel-filter-parent')

/* Usage
---------------------- */

export { fnAppend1, fnAppend1R }
