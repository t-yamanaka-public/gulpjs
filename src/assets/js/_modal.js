
/* Import
---------------------- */
import { overlay2 } from './_overlay'
import { scrollControl } from './_scroll-control'

/* Variable
---------------------- */
let thumbCount
let elmIndex
let favoriteTriggerElm
let favoriteTriggerElms
const favoriteList = document.getElementById('favorite-list')

/* Class
---------------------- */
class Modal {
  constructor (trigger, modal, modalImg, closeButton, pagerPrev, pagerNext) {
    this.trigger = trigger
    this.triggers = document.querySelectorAll(trigger)
    this.modal = document.querySelector(modal)
    this.modalImg = document.querySelector(modalImg)
    this.closeButton = document.querySelector(closeButton)
    this.pagerPrev = document.querySelector(pagerPrev)
    this.pagerNext = document.querySelector(pagerNext)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    // サムネイルの初期設定
    this.triggers.forEach((element, index, array) => {
      element.dataset.index = index // data-indexを設定する
      thumbCount = array.length // 個数を取得
    })
  }

  // モーダルを閉じる処理
  closeModal (modal) {
    modal.setAttribute('aria-hidden', 'true')
    overlay2.hide() // Overlayを非表示
    scrollControl.release() // スクロール固定を解除
    this.modalImg.src = ''
  }

  // ページング
  paging (event, destTarget) {
    event.preventDefault()
    this.main(event, destTarget)
  }

  // メイン関数
  main (event, destTarget) {
    let target
    /* -- ページャーをクリックしたとき -- */
    if (destTarget) {
      target = destTarget
    /* -- サムネイルをクリックしたとき -- */
    } else {
      /* -- お気に入りページ -- */
      if (favoriteList) {
        target = event.target.closest(this.trigger)
      /* -- お気に入りページ以外 -- */
      } else {
        target = event.currentTarget
      }
    }

    // モーダルを表示
    this.modal.setAttribute('aria-hidden', 'false')

    // ページャーdisabled
    if (this.pagerPrev) {
      this.pagerPrev.disabled = false
      if (Number(target.dataset.index) === 0) {
        this.pagerPrev.disabled = true
      }
    }
    if (this.pagerNext) {
      if (favoriteList) {
        thumbCount = favoriteTriggerElms.length
      }
      this.pagerNext.disabled = false
      if (Number(target.dataset.index) === thumbCount - 1) {
        this.pagerNext.disabled = true
      }
    }

    // サムネイルの画像パスをモーダルに表示
    const imgPath = target.querySelector('img').getAttribute('src')

    this.modalImg.src = imgPath.replace(/thumb/i, 'preview') // サムネイル画像とプレビュー画像のディレクトリを分ける場合
    // this.modalImg.src = imgPath

    // サムネイルのデザインIDをモーダルに表示
    const designId = target.dataset.design
    this.modal.querySelector('.modal-caption__id').innerHTML = designId
    if (this.modal.querySelector('.c-button-b__target')) {
      this.modal.querySelector('.c-button-b__target').dataset.design = designId
    }

    // サムネイルのfavoriteアイコンの選択状態をモーダルに反映
    const favoriteSelected = target.querySelector('.c-icon-favorite').classList.contains('is-selected')
    const favoriteModal = this.modal.querySelector('.c-icon-favorite')
    if (favoriteSelected) {
      favoriteModal.classList.add('is-selected')
    } else {
      favoriteModal.classList.remove('is-selected')
    }

    overlay2.show() // Overlayを表示
    scrollControl.fixed() // スクロールの固定
  }

  // イベントの登録
  addListener (swiperInitData) {
    /* -- swiper初期化後のデータがあればそれを使う -- */
    if (swiperInitData) {
      this.triggers = swiperInitData.targets
    }
    /* -- サムネイルをclickしたとき（お気に入りページ） -- */
    if (favoriteList) {
      favoriteList.addEventListener('click', (event) => {
        event.preventDefault()
        if (!event.target.closest(this.trigger)) return false
        favoriteTriggerElm = event.target.closest(this.trigger) // clickした要素
        favoriteTriggerElms = event.currentTarget.querySelectorAll(this.trigger) // triggerのNodeList
        favoriteTriggerElms.forEach((element, index) => {
          if (favoriteTriggerElm === element) {
            elmIndex = index // clickした要素のindex
          }
          element.dataset.index = index // 各要素にdata-index付与
        })
        this.main(event)
      }, false)
    /* -- サムネイルをclickしたとき（お気に入りページ以外） -- */
    } else {
      this.triggers.forEach((element, index, array) => {
        element.addEventListener('click', (event) => {
          event.preventDefault()
          elmIndex = index
          this.main(event)
        }, false)
      })
    }
    /* -- Closeボタンをclickしたとき -- */
    this.closeButton.addEventListener('click', () => {
      this.closeModal(this.modal)
    }, false)
    /* -- Overlayをclickしたとき -- */
    overlay2.overlayElm.addEventListener('click', () => {
      this.closeModal(this.modal)
    }, false)
    /* -- モーダルのぺージャー（prev）をclickしたとき -- */
    if (this.pagerPrev) {
      this.pagerPrev.addEventListener('click', (event) => {
        if (favoriteList) {
          // 一つ前のtriggerを取得（お気に入りページ）
          elmIndex--
          const destTarget = favoriteTriggerElms[elmIndex]
          this.paging(event, destTarget)
        } else {
          // 一つ前のtriggerを取得（お気に入りページ以外）
          for (let i = 0; i < this.triggers.length; i++) {
            const element = this.triggers[i]
            if (Number(element.dataset.index) === elmIndex - 1) {
              const destTarget = element
              this.paging(event, destTarget)
              elmIndex = i
              break
            }
          }
        }
      }, false)
    }
    /* -- モーダルのぺージャー（next）をclickしたとき -- */
    if (this.pagerNext) {
      this.pagerNext.addEventListener('click', (event) => {
        if (favoriteList) {
          // 一つ次のtriggerを取得（お気に入りページ）
          elmIndex++
          const destTarget = favoriteTriggerElms[elmIndex]
          this.paging(event, destTarget)
        } else {
          // 一つ次のtriggerを取得（お気に入りページ以外）
          for (let i = 0; i < this.triggers.length; i++) {
            const element = this.triggers[i]
            if (Number(element.dataset.index) === elmIndex + 1) {
              const destTarget = element
              this.paging(event, destTarget)
              elmIndex = i
              break
            }
          }
        }
      }, false)
    }
  }

  // 実行
  run (swiperInitData) {
    if (!this.modal) return false
    this.prepare()
    this.init()
    this.addListener(swiperInitData)
  }
}

// インスタンス作成
const fnModal = new Modal(
  '.p-thumb-a__target', // サムネイルのリンク
  '.p-panel-modal', // モーダル
  '.p-panel-modal__img', // モーダルに表示するimgタグ
  '.p-panel-modal__closebutton', // モーダルを閉じるボタン
  '.modal-caption__pager--prev .c-link-s__target', // ページャー（前へ）
  '.modal-caption__pager--next .c-link-s__target' // ページャー（次へ）
)

// エクスポート
export { fnModal }
