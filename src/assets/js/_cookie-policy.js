
/* Import
---------------------- */
import Cookies from 'js-cookie'

/* Variable
---------------------- */
const band = document.getElementById('band-cookie')
const button = document.getElementById('cookie-button')

/* Function
---------------------- */
const cookiePolicy = (event) => {
  // when load
  if (button) {
    const privacyCheck = Cookies.get('privacyCheck')
    if (privacyCheck !== 'done') {
      band.setAttribute('aria-hidden', 'false')
    }
    // when click
    button.addEventListener('click', function (event) {
      band.setAttribute('aria-hidden', 'true')
      Cookies.set('privacyCheck', 'done', { samesite: 'lax' })
    }, false)
  }
}

export { cookiePolicy }
