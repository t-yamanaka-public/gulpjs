export const xhr = (type, url, send) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.open(type, url, true)
    xhr.withCredentials = true
    xhr.send(send)
    xhr.onload = () => {
      if (xhr.readyState === 4 && xhr.status === 200) {
        resolve(xhr)
      } else {
        reject(xhr)
      }
    }
    xhr.onerror = () => {
      reject(xhr)
    }
  })
}
