
// require('desvg')
import 'desvg'
import 'svgxuse'
import platform from 'platform'
import objectFitImages from 'object-fit-images'
import 'what-input'
import smoothscroll from 'smoothscroll-polyfill'
import picturefill from 'picturefill'
import 'element-closest-polyfill'
import ScrollHint from 'scroll-hint'

export default () => {
  /* Variable
  ---------------------- */
  const HTML = document.querySelector('html')

  /* Execution
  ---------------------- */
  // object-fit-images
  objectFitImages()

  // deSVG
  deSVG('.c-desvg', true)

  // platform
  if (platform.name === 'IE' && platform.version <= 11) {
    HTML.classList.add('ie')
  }
  if (platform.name === 'Microsoft Edge') {
    HTML.classList.add('edge')
  }
  if (platform.name === 'Firefox') {
    HTML.classList.add('firefox')
  }
  if (platform.name === 'Safari') {
    HTML.classList.add('safari')
    /* -- PCのSafariのversion14未満 -- */
    if (parseFloat(platform.version) < 14 && platform.product !== 'iPhone') {
      HTML.classList.add('less14')
    }
  }
  if (platform.os.family === 'Windows') {
    HTML.classList.add('windows')
  }
  if (/Android/.test(platform.os)) {
    HTML.classList.add('android')
  }
  if (/Android/.test(platform.os) && platform.version <= 4) {
    HTML.classList.add('android4')
  }

  // isTouchDevice
  function isTouchDevice () {
    let result = false
    if (window.ontouchstart === null) {
      result = true
    }
    return result
  }
  if (isTouchDevice()) {
    HTML.classList.add('touch-device')
  } else {
    HTML.classList.add('not-touch-device')
  }

  // Smooth Scroll behavior polyfill for IE
  smoothscroll.polyfill()

  // A responsive image polyfill for IE
  picturefill()

  // ScrollHint
  new ScrollHint('.js-scrollable', {
    i18n: {
      scrollable: 'スクロールできます'
    }
  })
}
