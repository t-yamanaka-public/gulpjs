
/* Import
---------------------- */
// 実行するaccordionのオブジェクトをimport
import { accordion3 } from './_accordion'

/* Usage
---------------------- */
/*
closePanelを実行したい要素にjs-close-panelクラスを付ける
// a.c-button-c__target.js-close-panel(href="")
*/

/* Variable
---------------------- */

/* Class
---------------------- */
class ClosePanel {
  constructor (target, accordion) {
    this.targets = document.querySelectorAll(target)
    this.accordion = accordion
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
  }

  // メイン関数
  main (event) {
    // const target = event.currentTarget
    event.preventDefault()
    this.accordion.closeAll()
  }

  // イベントの登録
  addListener () {
    if (!this.targets) return false
    this.targets.forEach((element, index) => {
      element.addEventListener('click', this.main, false)
    })
  }

  // 実行
  run () {
    this.prepare()
    // this.init()
    this.addListener()
  }
}

// const closePanel = new ClosePanel('クリックする要素のクラス名', 実行するaccordionのオブジェクト)
const closePanel3 = new ClosePanel('.js-close-panel', accordion3)

export { closePanel3 }
