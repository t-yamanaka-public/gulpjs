
/* Import
---------------------- */

/* Variable
---------------------- */
const checkedClass = 'is-checked'

/* Class
---------------------- */
class ToggleCheckbox {
  constructor (target) {
    this.targets = document.querySelectorAll(target)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    this.targets.forEach((element) => {
      if (element.querySelector('input')) {
        /* -- checkboxがチェックされていたら -- */
        if (element.querySelector('input').checked) {
          element.classList.add(checkedClass)
        }
      }
    })
  }

  // メイン関数
  main (event) {
    const target = event.currentTarget

    if (target.classList.contains(checkedClass)) {
      target.classList.remove(checkedClass)
    } else {
      target.classList.add(checkedClass)
    }
  }

  // イベントの登録
  addListener () {
    this.targets.forEach((element, index) => {
      /* -- 子要素にinputがあるときは -- */
      if (element.control) {
        // inputのイベント伝播を止める
        element.control.addEventListener('click', (event) => {
          event.stopPropagation()
        }, false)
      }
      // メイン関数のリスナー
      element.addEventListener('click', this.main, false)
    })
  }

  // イベントの解除
  // removeListener () {
  // }

  // 実行
  run () {
    this.prepare()
    this.init()
    this.addListener()
  }
}

// Normal
const fnToggleCheckbox1 = new ToggleCheckbox('#toggle-checkbox-1 .c-button-c__target')

export { fnToggleCheckbox1 }
