
/* Variable
---------------------- */
const header = document.getElementById('header')
const heroHeight = document.getElementById('hero').clientHeight
const invertClass = 'l-header--inversion'
const fixedClass = 'is-fixed'
const animClass = 'anim'
const topClass = 'is-top'

/* Function
---------------------- */
const fnHeader = {

  run: function () {
    this.addListener()
    this.func()
  },

  func: function () {
    const scrollTop = document.documentElement.scrollTop

    /* scrollTopが50pxより小さいとき */
    if (scrollTop < 50) {
      header.classList.add(topClass)
    } else {
      header.classList.remove(topClass)
    }

    /* scrollTopがheroHeightより大きいとき */
    if (scrollTop > heroHeight) {
      header.classList.add(animClass)
    /* scrollTopがheroHeightより小さいとき */
    } else {
      header.classList.remove(animClass)
    }

    /* scrollTopがheroHeightの半分より大きいとき */
    if (scrollTop > heroHeight / 2) {
      header.classList.add(invertClass)
      header.classList.add(fixedClass)

    /* scrollTopがheroHeightの半分より小さいとき */
    } else {
      header.classList.remove(invertClass)
      header.classList.remove(fixedClass)
    }
  },

  addListener: function () {
    window.addEventListener('scroll', this.func, false)
  }
}

export { fnHeader }
