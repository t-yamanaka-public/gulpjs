
/* Import
---------------------- */

/* Variable
---------------------- */
const selectedClass = 'is-selected'
const bandFavorite = document.getElementById('band-favorite')
const favoriteList = document.getElementById('favorite-list')
const localStorageKey = 'favorite'
let dataDesignArr = []
let favoriteListHTML = ''

/* Class
---------------------- */
class Favorite {
  constructor (icon, trigger, modal, iconModal) {
    this.icon = icon
    this.iconElm = null
    this.iconElms = null
    this.trigger = trigger
    this.triggerElms = null
    this.modal = modal
    this.iconModal = iconModal
  }

  // 準備
  prepare () {
    this.iconElm = document.querySelector(this.icon)
    this.iconElms = document.querySelectorAll(this.icon)
    this.triggerElms = document.querySelectorAll(this.trigger)
  }

  // 初期化
  init () {
    if (!this.iconElm && !favoriteList) return false

    // localStorageのdata-designをサムネイルに反映す
    if (localStorage.getItem(localStorageKey)) {
      const triggerElms = document.querySelectorAll(this.trigger) // サムネイルのNodeList
      const targetIcons = document.querySelectorAll(this.icon) // favoriteアイコンのNodeList
      targetIcons.forEach((element) => {
        element.addEventListener('click', (event) => {
          event.preventDefault()
          event.stopPropagation()
        })
      })
      dataDesignArr = JSON.parse(localStorage.getItem(localStorageKey))

      dataDesignArr.forEach((favoriteDataDesign, index) => {
        triggerElms.forEach((trigger, index) => {
          /* -- localStorageのdata-designとサムネイルのdata-designが一致すれば -- */
          if (favoriteDataDesign === trigger.dataset.design) {
            // アイコンを選択する
            trigger.querySelector(this.icon).classList.add(selectedClass)
          }
        })
      })
    }

    this.stopPropagation()
    this.toggleIcon(this.icon)
    this.toggleIcon(this.iconModal)
    this.applyThumb()
  }

  // favoriteアイコンの伝播を止める
  stopPropagation () {
    this.iconElms.forEach((element) => {
      element.addEventListener('click', (event) => {
        event.preventDefault()
        event.stopPropagation()
      })
    })
  }

  // favoriteアイコンのトグル処理
  toggleIcon (icon) {
    const icons = document.querySelectorAll(icon)
    let timeout

    /* -- favoriteアイコンをclickしたとき -- */
    icons.forEach((icon) => {
      icon.addEventListener('click', (event) => {
        const targetIcon = event.currentTarget // clickしたアイコン
        let dataDesign

        /* -- clickしたのがサムネイルのアイコンのとき -- */
        if (targetIcon.closest(this.trigger)) {
          dataDesign = targetIcon.closest(this.trigger).dataset.design // clickしたサムネイルのdata-design
        /* -- clickしたのがモーダルのアイコンのとき -- */
        } else if (targetIcon.closest(this.modal)) {
          dataDesign = targetIcon.closest(this.modal).querySelector('.modal-caption__id').innerText
        } else {
          console.log('対象外のclick')
        }

        /* -- アイコンが選択されていたら -- */
        if (targetIcon.classList.contains(selectedClass)) {
          if (targetIcon.closest(this.trigger)) {
            icons.forEach((c) => {
              if (targetIcon.closest(this.trigger).dataset.design === c.closest(this.trigger).dataset.design) {
                c.classList.remove(selectedClass) // 選択解除する
              }
            })
          } else {
            targetIcon.classList.remove(selectedClass) // 選択解除する
          }

          // localStorageからdata-designを削除
          dataDesignArr = JSON.parse(localStorage.getItem(localStorageKey))
          dataDesignArr.forEach((element, index) => {
            /* -- localStorageのdata-designとclickしたdata-designが一致すれば -- */
            if (element === dataDesign) {
              // localStorage内の一致した要素を削除する
              dataDesignArr.splice(index, 1)
              localStorage.setItem(localStorageKey, JSON.stringify(dataDesignArr))
            }
          })
          // お気に入りから外しました
          bandFavorite.classList.remove('is-added')
          bandFavorite.classList.add('is-removed')

        /* -- アイコンが選択されていなければ -- */
        } else {
          if (targetIcon.closest(this.trigger)) {
            icons.forEach((c) => {
              if (targetIcon.closest(this.trigger).dataset.design === c.closest(this.trigger).dataset.design) {
                c.classList.add(selectedClass) // 選択する
              }
            })
          } else {
            targetIcon.classList.add(selectedClass) // 選択する
          }

          // localStorageにdata-designを保存
          dataDesignArr.push(dataDesign)
          localStorage.setItem(localStorageKey, JSON.stringify(dataDesignArr))
          // お気に入りに保存しました
          bandFavorite.classList.remove('is-removed')
          bandFavorite.classList.add('is-added')
        }

        // お気に入り保存オビの表示・非表示
        bandFavorite.setAttribute('aria-hidden', 'false')
        clearTimeout(timeout)
        timeout = setTimeout(() => {
          bandFavorite.setAttribute('aria-hidden', 'true')
        }, 3500)
      }, false)
    })
  }

  // モーダルのfavoriteアイコンの選択状態をサムネイルに反映
  applyThumb () {
    const iconModal = document.querySelector('.p-panel-modal__favorite .c-icon-favorite')
    const designIdElm = document.querySelector('.modal-caption__id')
    iconModal.addEventListener('click', (event) => {
      const designId = designIdElm.textContent

      const thumbTargets = document.querySelectorAll(this.trigger)
      thumbTargets.forEach((element) => {
        /* -- サムネイルのdata-designとモーダルのcaptionのidが同じとき -- */
        if (element.dataset.design === designId) {
          const targetIcon = element.querySelector('.p-thumb-a__favorite .c-icon-favorite')
          /* -- アイコンが選択されていたら -- */
          if (targetIcon.classList.contains(selectedClass)) {
            targetIcon.classList.remove(selectedClass) // 選択解除する
          /* -- アイコンが選択されていなければ -- */
          } else {
            targetIcon.classList.add(selectedClass) // 選択する
          }
        }
      })
    }, false)
  }

  favoriteList () {
    // localStorageのdata-designからサムネイルを生成する
    if (!localStorage.getItem(localStorageKey)) return false
    dataDesignArr = JSON.parse(localStorage.getItem(localStorageKey))
    dataDesignArr.forEach((dataDesign, index, array) => {
      const triggerTemp =
      `<li class="c-grid__col">
           <div class="p-thumb-a">
             <a class="p-thumb-a__target designDetail" href="#" data-design=${dataDesign}>
               <div class="p-thumb-a__thumb">
                 <img src="../../mypage/images/postcard/thumb/${dataDesign}.jpg" data-psrc="../../mypage/images/postcard/preview/${dataDesign}.jpg" width="179" height="265" loading="lazy" decoding="async" alt="">
               </div>
               <div class="p-thumb-a__favorite">
                 <span class="c-icon-favorite">
                   <svg class="c-icon-a red" role="img">
                     <use xlink:href="/nenga/assets/img/svg-sprite/svg-icons.svg#ico_heart_02"></use>
                   </svg>
                   <svg class="c-icon-a" role="img">
                     <use xlink:href="/nenga/assets/img/svg-sprite/svg-icons.svg#ico_heart_01"></use>
                   </svg>
                 </span>
               </div>
             </a>
           </div>
         </li>`
      favoriteListHTML += triggerTemp
    })
    favoriteList.innerHTML = favoriteListHTML

    if (document.getElementById('favNumber')) {
      document.getElementById('favNumber').innerHTML = dataDesignArr.length
      if (dataDesignArr.length === 0) {
        document.querySelector('.p-list-thumb__shoulder').classList.add('is-nodata')
      }
    }
  }

  // 実行
  run () {
    this.prepare()
    if (favoriteList) {
      this.favoriteList()
    }
    this.init()
  }
}

const fnFavorite = new Favorite(
  '.p-thumb-a__favorite .c-icon-favorite', // サムネイルのfavoritアイコン
  '.p-thumb-a__target', // サムネイルのtrigger
  '.p-panel-modal', // モーダル
  '.p-panel-modal__favorite .c-icon-favorite' // モーダルのfavoriteアイコン
)

export { fnFavorite }
