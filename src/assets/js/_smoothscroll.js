
/* Variable
---------------------- */

/* Function
---------------------- */
const smoothscroll = function () {
  window.addEventListener('load', function () {
    const nodelist = document.querySelectorAll('a[href^="#"]')
    const node = Array.prototype.slice.call(nodelist, 0)
    const pathname = location.pathname

    // URLに含まれるときはscroll-behaviorをautoにする
    if (pathname.indexOf('contact') >= 0) {
      document.querySelector('html').style.scrollBehavior = 'auto'
    }

    node.forEach(function (elem, index) {
      elem.addEventListener('click', function (e) {
        e.preventDefault()

        const href = elem.getAttribute('href')

        document.querySelector(href).scrollIntoView({ behavior: 'smooth' })
      })
    })
  })
  // Remove Hash
  window.onhashchange = () => {
    if (window.location.hash === '#wrapper') {
      window.history.replaceState(null, '', window.location.pathname + window.location.search)
    }
  }
}

export { smoothscroll }
