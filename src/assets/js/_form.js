
/* Import
// ---------------------- */
import fetchJsonp from 'fetch-jsonp'

/* Variable
---------------------- */

/* Function
---------------------- */
// 郵便番号検索
const postalCode = () => {
  const api = 'https://zipcloud.ibsnet.co.jp/api/search?zipcode='
  const error = document.getElementById('postal-error')
  const input = document.getElementById('postal-input')
  const address1 = document.getElementById('postal-address1')

  input.addEventListener('input', function () {
    const param = input.value.replace('-', '') // ハイフン削除
    const url = api + param

    fetchJsonp(url, {
      timeout: 10000 // タイムアウト
    })
      .then((response) => {
        error.textContent = '' // エラーメッセージ初期化
        return response.json() // レスポンスデータ取り出し
      })
      .then((data) => {
        if (param.length >= 7) { // 7ケタ入力されたら実行
          if (data.status === 400) { // エラー時
            error.textContent = data.message
          } else if (data.results === null) {
            error.textContent = '郵便番号から住所が見つかりませんでした'
          } else {
            const addr1 = data.results[0].address1
            const addr2 = data.results[0].address2
            const addr3 = data.results[0].address3
            address1.value = addr1 + addr2 + addr3
          }
        }
      })
      .catch((ex) => { // 例外処理
        console.log(ex)
      })
  }, false)
  // ケタ数が足りない時のエラー処理
  input.addEventListener('blur', function () {
    const param = input.value.replace('-', '') // ハイフン削除
    const url = api + param

    fetchJsonp(url)
      .then((response) => {
        error.textContent = '' // エラーメッセージ初期化
        return response.json() // レスポンスデータ取り出し
      })
      .then((data) => {
        if (param.length < 7) { // 7ケタ未満の時
          if (data.status === 400) { // エラー時
            error.textContent = data.message
          }
        }
      })
      .catch((ex) => { // 例外処理
        console.log(ex)
      })
  }, false)
}

// 同意しましたチェックボックス
const agree = () => {
  const agreeCheckbox = document.getElementById('agree-checkbox')
  const confirmButton = document.getElementById('confirm-button')
  const disabledClass = 'disabled'
  if (!agreeCheckbox) return false

  agreeCheckbox.addEventListener('click', function (e) {
    if (!this.checked) {
      confirmButton.classList.add(disabledClass)
    } else {
      confirmButton.classList.remove(disabledClass)
    }
  }, false)
}

// 「個人」を選択すると「会社名」を非表示に
const organization = () => {
  const orgRadios = document.querySelectorAll('.org-radio')
  const orgRadiosArr = Array.prototype.slice.call(orgRadios, 0)
  const companyUnit = document.getElementById('company-unit')

  orgRadiosArr.forEach((element, index) => {
    element.addEventListener('click', function (e) {
      if (this.id === 'org-radio-personal') {
        companyUnit.setAttribute('aria-hidden', 'true')
      } else {
        companyUnit.setAttribute('aria-hidden', 'false')
      }
    }, false)
  })
}

/* Export
---------------------- */
export { postalCode, agree, organization }
