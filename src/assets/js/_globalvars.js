
/* Constant Variables
---------------------- */
export const TRUE = 'true'
export const FALSE = 'false'
export const BREAKP = 736
export const HIDDEN = 'aria-hidden'
export const EXPANDED = 'aria-expanded'
export const SELECTED = 'aria-selected'
