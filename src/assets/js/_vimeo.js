
/* Import
---------------------- */
import Vimeo from '@vimeo/player'

/* Variable
---------------------- */
const closes = document.querySelectorAll('[data-micromodal-close]')
const closeArr = Array.prototype.slice.call(closes, 0)
const thumbs = document.querySelectorAll('.c-media-a--video .c-media-a__thumb, .c-media-b--video .c-media-b__thumb')
const thumbArr = Array.prototype.slice.call(thumbs, 0)
const vimeoIframes = document.querySelectorAll('.vimeo-iframe')
const vimeoIframeArr = Array.prototype.slice.call(vimeoIframes, 0)

/* Function
---------------------- */
const fnVimeo = () => {
  // vimeoのサムネイルを取得してセット
  vimeoIframeArr.forEach((element, index) => {
    const vimeoUrl = element.getAttribute('src')
    console.log(vimeoUrl)
    let result = vimeoUrl.match(/video\/\d+\?/gi)
    if (result) {
      result = result[0].split(/[/?]/)
      const vimeoId = result[1]
      const xhr = new XMLHttpRequest()
      xhr.open('GET', `https://vimeo.com/api/oembed.json?url=https://vimeo.com/${vimeoId}`)
      xhr.send()

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4 && xhr.status === 200) {
          if (xhr.response) {
            const jsonObj = JSON.parse(xhr.responseText)
            const thumbUrl = jsonObj.thumbnail_url
            thumbArr[index].setAttribute('src', thumbUrl)
          }
        }
      }
    }
  })
  // モーダルを閉じたらvimeoをpauseする
  const clickEventType = ((window.ontouchstart !== null) ? 'click' : 'touchend')
  closeArr.forEach((element, index) => {
    element.addEventListener('touchstart', function (e) {
      e.stopPropagation()
    })
    element.addEventListener(clickEventType, function (e) {
      vimeoIframeArr.forEach((element2, index) => {
        const vimeoUrl = element2.getAttribute('src')
        const video = vimeoUrl.match(/video\/\d+\?/gi)
        if (video) {
          const player = new Vimeo(element2)
          player.pause()
        }
      })
    })
  })
}

export { fnVimeo }
