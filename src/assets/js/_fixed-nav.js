
/* Variable
---------------------- */
const nav = document.getElementById('fixed-nav')
const navTargets = document.querySelectorAll('.p-nav-c__target')

/* Function
---------------------- */
const fnFixedNav = {
  run: function () {
    if (!nav) return false
    this.addListener()
    this.func()
  },

  func: function () {
    const winH = window.innerHeight
    const meritPos = document.getElementById('merit').getBoundingClientRect().top
    const comparePos = document.getElementById('compare').getBoundingClientRect().top
    const strengthsPos = document.getElementById('strengths').getBoundingClientRect().top
    const casePos = document.getElementById('case').getBoundingClientRect().top
    const contactPos = document.getElementById('footer-contact').getBoundingClientRect().top
    const current = element => element.setAttribute('aria-current', 'page')
    const removeCurrent = element => element.removeAttribute('aria-current')

    /* -- #meritが画面の1/3より上にあるとき -- */
    if (meritPos < winH - (winH / 3)) {
      // navを表示
      nav.setAttribute('aria-hidden', 'false')
      // メリットをカレント表示
      navTargets.forEach((element, index) => {
        if (element.hash === '#merit') {
          current(element)
        } else {
          // それ以外のカレントを削除
          removeCurrent(element)
        }
      })
      /* -- #compareが画面の1/3より上にあるとき -- */
      if (comparePos < winH - (winH / 3)) {
        // 活用比較をカレント表示
        navTargets.forEach((element, index) => {
          if (element.hash === '#compare') {
            current(element)
          } else {
            removeCurrent(element)
          }
        })
        /* -- #strengthsが画面の1/3より上にあるとき -- */
        if (strengthsPos < winH - (winH / 3)) {
          // 活用比較をカレント表示
          navTargets.forEach((element, index) => {
            if (element.hash === '#strengths') {
              current(element)
            } else {
              removeCurrent(element)
            }
          })
          /* -- #caseが画面の1/3より上にあるとき -- */
          if (casePos < winH - (winH / 3)) {
            // 活用比較をカレント表示
            navTargets.forEach((element, index) => {
              if (element.hash === '#case') {
                current(element)
              } else {
                removeCurrent(element)
              }
            })
            /* -- #footer-contactが画面最下より上にあるとき -- */
            if (contactPos < winH) {
              // navを非表示
              nav.setAttribute('aria-hidden', 'true')
            }
          }
        }
      }
    } else {
      // navを非表示
      nav.setAttribute('aria-hidden', 'true')
    }
  },

  addListener: function () {
    window.addEventListener('scroll', this.func, false)
  }
}

export { fnFixedNav }
