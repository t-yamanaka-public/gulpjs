
/* Import
---------------------- */

/* Variable
---------------------- */
const html = document.querySelector('html')
const wrapper = document.getElementById('wrapper')
const footer = document.getElementById('footer-wrap')
let position = 0

/* Class
---------------------- */
class ScrollDirection {
  constructor (upClass, downClass) {
    this.upClass = upClass
    this.downClass = downClass
  }

  // 準備
  prepare () {
    // this.main()中のthisをClassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
  }

  // メイン関数
  main () {
    const winH = window.innerHeight
    const scrollTop = document.documentElement.scrollTop
    const footerH = footer.clientHeight
    const pageH = wrapper.clientHeight

    /* -- 下へスクロールしたとき -- */
    if (position < document.documentElement.scrollTop) {
      // htmlにdownClassを付加
      html.classList.remove(this.upClass)
      html.classList.add(this.downClass)
    /* -- 上へスクロールしたとき -- */
    } else {
      /* -- footerより上部を表示しているとき -- */
      if (pageH - (winH + footerH) > scrollTop) {
        // htmlにupClassを付加
        html.classList.remove(this.downClass)
        html.classList.add(this.upClass)
      }
    }
    position = document.documentElement.scrollTop
  }

  // イベントの登録
  addListener () {
    document.addEventListener('scroll', this.main, false)
  }

  // イベントの解除
  removeListener () {
  }

  // 実行
  run () {
    this.prepare()
    // this.init()
    this.addListener()
  }
}

const fnScrollDirection = new ScrollDirection('scroll-up', 'scroll-down')

/* Usage
----------------------
button.c-toggler.js-toggler
  ...
.l-drawer#drawer
  .l-drawer__inner
    ...
*/

export { fnScrollDirection }
