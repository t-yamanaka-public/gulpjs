
/* Import
---------------------- */

/* Variable
---------------------- */
const selectedClass = 'is-selected'

/* Class
---------------------- */
class ToggleRadio {
  constructor (target) {
    this.targets = document.querySelectorAll(target)
  }

  // 準備
  prepare () {
    // this.main()中のthisをclassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
  }

  // メイン関数
  main (event) {
    const target = event.currentTarget

    /* -- radioボタンのとき -- */
    if (target.querySelector('input').type === 'radio') {
      this.targets.forEach((element) => {
        // すべて選択解除する
        element.classList.remove(selectedClass)
        element.setAttribute('aria-current', 'false')
      })
      // クリックした要素を選択状態にする
      target.classList.add(selectedClass)
      target.setAttribute('aria-current', 'true')
    }
  }

  // イベントの登録
  addListener () {
    this.targets.forEach((element, index) => {
      /* -- 子要素にinputがあるときは -- */
      if (element.control) {
        // inputのイベント伝播を止める
        element.control.addEventListener('click', (event) => {
          event.stopPropagation()
        }, false)
      /* -- 子要素にinputがないときは実行しない -- */
      } else {
        return false
      }
      // メイン関数のリスナー
      element.addEventListener('click', this.main, false)
    })
  }

  // イベントの解除
  // removeListener () {
  // }

  // 実行
  run () {
    this.prepare()
    // this.init()
    this.addListener()
  }
}

// Normal
const fnToggleRadio1 = new ToggleRadio('#toggle-radio-1 .c-button-c__target')

export { fnToggleRadio1 }
