
/* Import
---------------------- */
import Swiper from 'swiper'
import { fnModal } from './_modal'

/* Variable
---------------------- */
const swiper01 = document.getElementById('swiper-container-01')
const swiper02 = document.getElementById('swiper-container-02')
const swiperKV = document.getElementById('swiper-container-kv')

/* Function
---------------------- */
const fnSwiper01 = () => {
  // トップ おすすめ年賀状デザイン
  if (swiper01) {
    const mySwiper01 = new Swiper(swiper01, {
      // autoplay: {
      //   delay: 5000
      // },
      // effect: 'fade',
      loop: true,
      loopAdditionalSlides: 1,
      navigation: {
        nextEl: '.p-button-arrow--right',
        prevEl: '.p-button-arrow--left'
      },
      on: {
        init: function () {
          // swiper初期化後に要素を取得してmodalへ渡す
          const swiperInitData = {
            targets: document.querySelectorAll('#swiper-container-01 .p-thumb-a__target')
          }
          fnModal.run(swiperInitData)
        }
      },
      // pagination: {
      //   // el: '.swiper-pagination',
      //   clickable: true
      // },
      slidesPerView: 3,
      spaceBetween: 15,
      speed: 500,
      breakpoints: {
        // SP
        767: {
          slidesPerView: 1,
          spaceBetween: 10
        }
      }
    })
  }
}

const fnSwiper02 = () => {
  // トップ スタッフ一押しランキング
  if (swiper02) {
    const mySwiper02 = new Swiper(swiper02, {
      // autoplay: {
      //   delay: 5000
      // },
      // effect: 'fade',
      loop: true,
      loopAdditionalSlides: 1,
      navigation: {
        nextEl: '.p-button-arrow--right',
        prevEl: '.p-button-arrow--left'
      },
      on: {
        init: function () {
          // swiper初期化後に要素を取得してmodalへ渡す
          const swiperInitData = {
            targets: document.querySelectorAll('#swiper-container-02 .p-thumb-a__target')
          }
          fnModal.run(swiperInitData)
        }
      },
      // pagination: {
      //   // el: '.swiper-pagination',
      //   clickable: true
      // },
      slidesPerView: 3,
      spaceBetween: 15,
      speed: 500,
      breakpoints: {
        // SP
        767: {
          slidesPerView: 1,
          spaceBetween: 10
        }
      }
    })
  }
}

const fnSwiperKV = () => {
  // トップ KV
  if (swiperKV) {
    /* -- スライダーが1枚のとき -- */
    if (swiperKV.querySelectorAll('.swiper-slide').length === 1) {
      return false
    } else {
      const mySwiperKV = new Swiper(swiperKV, {
        autoplay: {
          delay: 5000
        },
        effect: 'fade',
        loop: true,
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
        speed: 1000
      })
    }
  }
}

// const fnSwiper02 = () => {
//   // トップMap
//   if (swiper02) {
//     const mySwiper02 = new Swiper(swiper02, {
//       initialSlide: 3,
//       loop: false,
//       navigation: {
//         nextEl: '.swiper-button-next',
//         prevEl: '.swiper-button-prev'
//       },
//       pagination: {
//         el: '.swiper-pagination',
//         clickable: true
//       }
//     })
//
//     const sendActiveSlideClass = () => {
//       const activeSliderElm = document.querySelector('.swiper-map .swiper-slide-active')
//       const activeSlideClass = activeSliderElm.className.match(/map-top-\w+/gi)[0]
//     }
//     sendActiveSlideClass()
//
//     mySwiper02.on('slideChangeTransitionStart', function () {
//       sendActiveSlideClass()
//     })
//   }
// }

export { fnSwiper01, fnSwiper02, fnSwiperKV }
