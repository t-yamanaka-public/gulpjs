
/* Import
---------------------- */

/* Variable
---------------------- */
const buttons = document.querySelectorAll('.p-fz-changer__button')
const buttonsArr = Array.prototype.slice.call(buttons, 0)
const htmlElm = document.getElementsByTagName('html').item(0)
const buttonLarge = document.getElementById('fz-large')

/* Function
---------------------- */
const fzChanger = () => {
  buttonsArr.forEach((element, index) => {
    element.addEventListener('click', function (e) {
      // Toggle current
      const current = this.getAttribute('aria-current')
      if (current !== 'true') {
        buttonsArr.forEach((element, index) => {
          element.setAttribute('aria-current', 'false')
        })
        this.setAttribute('aria-current', 'true')
      }
      // Add class to html
      if (this.id === 'fz-large') {
        // 「大きく」ボタンをクリックした時
        htmlElm.classList.add('fz-large')
        localStorage.setItem('fontsize', 'large')
      } else {
        // 「標準」ボタンをクリックした時
        htmlElm.classList.remove('fz-large')
        localStorage.setItem('fontsize', 'normal')
      }
    })
  })
  // localStorage
  const fontsize = localStorage.getItem('fontsize')
  if (fontsize === 'large') {
    htmlElm.classList.add('fz-large')
    buttonsArr.forEach((element, index) => {
      element.setAttribute('aria-current', 'false')
    })
    buttonLarge.setAttribute('aria-current', 'true')
  }
}

/* Execution
---------------------- */

/* Export
---------------------- */
export { fzChanger }
