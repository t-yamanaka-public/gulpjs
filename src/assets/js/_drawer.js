
/* Import
---------------------- */
import { overlay1 } from './_overlay'
import { scrollControl } from './_scroll-control'

/* Variable
---------------------- */

/* Class
---------------------- */
class Drawer {
  constructor (drawer, toggler, toggleText) {
    this.drawer = document.getElementById(drawer)
    this.toggleTexts = document.querySelectorAll(toggleText)
    this.togglers = document.querySelectorAll(toggler)
  }

  // 準備
  prepare () {
    // this.main()中のthisをClassのthisに固定する
    this.main = this.main.bind(this)
  }

  // 初期化
  init () {
    this.drawer.setAttribute('aria-hidden', 'true')
    this.togglers.forEach((element) => {
      element.setAttribute('aria-expanded', 'false')
      element.setAttribute('aria-controls', 'drawer')
    })
  }

  // パネルを開く処理
  open () {
    this.drawer.setAttribute('aria-hidden', 'false')
    this.togglers.forEach((element) => {
      element.setAttribute('aria-expanded', 'true')
    })
    // スクロールを止める
    scrollControl.fixed()
    // オーバーレイを表示
    overlay1.show()
    // toggleTextを変更
    this.toggleTexts.forEach((element) => {
      element.classList.add('is-open')
    })
  }

  // パネルを閉じる処理
  close () {
    this.drawer.setAttribute('aria-hidden', 'true')
    this.togglers.forEach((element) => {
      element.setAttribute('aria-expanded', 'false')
    })
    scrollControl.release()
    overlay1.hide()
    // toggleTextを変更
    this.toggleTexts.forEach((element) => {
      element.classList.remove('is-open')
    })
  }

  // メイン関数
  main () {
    this.togglers.forEach((element) => {
      const expanded = element.getAttribute('aria-expanded')

      /* -- パネルが閉じているとき -- */
      if (expanded === 'false') {
        this.open()
      /* -- パネルが開いているとき -- */
      } else {
        this.close()
      }
    })
  }

  // イベントの登録
  addListener () {
    this.togglers.forEach((element) => {
      element.addEventListener('click', this.main, false)
    })
    overlay1.overlayElm.addEventListener('click', () => {
      this.close() // パネルを閉じる
    })
  }

  // イベントの解除
  removeListener () {
    this.togglers.forEach((element) => {
      element.removeEventListener('click', this.main, false)
    })
  }

  // 実行
  run () {
    this.prepare()
    this.init()
    this.addListener()
  }
}

const fnDrawer = new Drawer('drawer', '.js-toggler', '.c-toggler__text')

/* Usage
----------------------
button.c-toggler.js-toggler
  ...
.l-drawer#drawer
  .l-drawer__inner
    ...
*/

export { fnDrawer }
