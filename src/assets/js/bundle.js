
/* Import
---------------------- */
import others from './_others'
import {
  fnAppend1,
  fnAppend1R
} from './_append'
import { fnAppends } from './_appends'
import { accordion1, accordion2, accordion3, accordion4 } from './_accordion'
import { closePanel3 } from './_close-panel'
// import { fnPageTop } from './_pagetop'
// import { smoothscroll } from './_smoothscroll'
import { fnDrawer } from './_drawer'
import { fnFavorite } from './_favorite'
import { fnScrollDirection } from './_scroll-direction'
import { fnFixedNav } from './_fixed-nav'
import { fnSwiper01, fnSwiper02, fnSwiperKV } from './_swiper'
import { fnToggleRadio1 } from './_toggle-radio'
import { fnToggleCheckbox1 } from './_toggle-checkbox'
import { fnModal } from './_modal'
import { fnSum1 } from './_sum'

/* Variable
---------------------- */
const BREAKP = 767
const mediaQuerySP = window.matchMedia(`(max-width: ${BREAKP}px)`)

/* Execution
---------------------- */
accordion2.run()
accordion3.run()
accordion4.run()
closePanel3.run()
fnAppends.run()
fnDrawer.run()
fnModal.run()
fnScrollDirection.run()
fnToggleRadio1.run()
fnToggleCheckbox1.run()
fnSwiper01()
fnSwiper02()
fnSwiperKV()
fnSum1.run()
fnFavorite.run()

// MatchMedia
const listener = (event) => {
  if (event.matches) {
    // SP
    fnAppend1.run()
    accordion1.run()
    fnFixedNav.run()
  } else {
    // PC
    fnAppend1R.run()
    accordion1.resetSP()
  }
}
// MatchMediaのみで発火
const listenerOnlyMatchMedia = (event) => {
  if (event.matches) {
    // SP
  } else {
    // PC
  }
}
mediaQuerySP.addListener(listener)
mediaQuerySP.addListener(listenerOnlyMatchMedia)
listener(mediaQuerySP)

// window.onload
window.onload = () => {
  others()
}
