
/* Import
---------------------- */

/* Variable
---------------------- */

/* Class
---------------------- */
class PreventDefault {
  constructor (target) {
    this.targets = document.querySelectorAll(target)
  }

  // 準備
  prepare () {
    // this.main()中のthisをClassのthisに固定する
    // this.main = this.main.bind(this)
  }

  // 初期化
  init () {
  }

  // メイン関数
  main (event) {
    // const target = event.currentTarget
    event.preventDefault()
  }

  // イベントの登録
  addListener () {
    this.targets.forEach((element, index) => {
      element.addEventListener('click', this.main, false)
    })
  }

  // イベントの解除
  removeListener () {
    this.targets.forEach((element, index) => {
      element.removeEventListener('click', this.main, false)
    })
  }

  // 実行
  run () {
    // this.prepare()
    // this.init()
    this.addListener()
  }
}

const preventGnav = new PreventDefault('.l-gnav__target')
const preventFlink = new PreventDefault('.flink-unit__dlist .flink-unit__target')

export { preventGnav, preventFlink }
